﻿using FaceRecon.models;
using FaceRecon.views;
using FaceRecon.ViewModels;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FaceRecon.views
{
    /// <summary>
    /// Interaction logic for LandingView.xaml
    /// </summary>
    public partial class LandingView : Page
    {
        public LandingView()
        {
            InitializeComponent();
        }

        private void Navigate(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            string name = btn.Name;


            switch (name)
            {
            case ("ImgLoader"):
                if(ShowImgViewModel.OpenFile() == true){
                    NavigationService?.Navigate(new ShowImg());
                }
                break;
            }

        }
    }
}

