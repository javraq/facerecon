﻿using FaceRecon.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.Diagnostics;
using System.Windows.Navigation;
using System.IO;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace FaceRecon.ViewModels
{
    class ShowImgViewModel
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private static string _path;

        public string FilePath
        {
            get { return _path; }
            set
            {
                if (_path != value)
                {
                    _path = value;
                    OnPropertyChanged();
                }
            }
        }
        public static bool OpenFile()
        {
            
            OpenFileDialog OpenFileDialog = new OpenFileDialog();
            OpenFileDialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            OpenFileDialog.CheckFileExists = true;
            OpenFileDialog.CheckPathExists = true;

            if (OpenFileDialog.ShowDialog() == true)
            {
                FileModel fileModel = new FileModel();
                _path = OpenFileDialog.FileName;
                Debug.WriteLine(fileModel.FilePath);
                return true;
            }
            else
            {
                return false;
            }
        }
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

